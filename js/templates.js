$(document).ready(function(){
    
    //Sample data for output in templates
    var view = {
        title : "Modal window title",
        content : "Modal window content"
    };
    
    //Load modal template 1
    $("#templates").load("./templates/modal_template1.html #modal_template1",function(){
        var template = document.getElementById('modal_template1').innerHTML;
        var output = Mustache.render(template, view);
        $("#modal_container1").html(output);
    });
    
    //Load modal template 2
    $("#templates").load("./templates/modal_template2.html #modal_template2",function(){
        var template = document.getElementById('modal_template2').innerHTML;
        var output = Mustache.render(template, view);
        $("#modal_container2").html(output);
    });
});